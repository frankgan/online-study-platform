
{{-- style --}}

<style>

  section#event-box {
    border: 1px solid #ccc;
    border-radius: 6px;
    overflow: hidden;
    transition: all .21s ease-in-out;
    cursor: pointer;
  }

  section#event-box:hover {
    box-shadow: 0 0 20px #888;
  }

  section#event-box > img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    max-height: 200px;
  }

  .date {
    color: #777;
    font-family: 'Open Sans', sans-serif;
    font-weight: normal;
    font-size: .9rem;
    padding-bottom: 1rem;
    border-bottom: 1px solid #bbb;
    margin-bottom: 1rem;
  }

  section#event-box .content {
    padding: 2rem;
  }



</style>

<section id="event-box">

  <img src="{{ $img }}" alt="">

  <div class="content">

    <h3>{{ $title }}</h3>
    <h4 class="date">{{ $date }}</h4>

    <p>{{ $slot }}</p>

  </div>

</section>