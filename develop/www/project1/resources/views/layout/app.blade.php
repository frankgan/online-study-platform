<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config("app.name") }} | @yield('title')</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <script type="module" src="https://5173-frankgan-onlinestudypla-v5sez2an3rv.ws-us110.gitpod.io/resources/js/app.js"></script>
        <link rel="stylesheet" href="https://5173-frankgan-onlinestudypla-v5sez2an3rv.ws-us110.gitpod.io/resources/css/app.css">
    
    </head>
    <body>

        @include('include.navbar')

        @yield('app')

        @include('include.footer')
    </body>
</html>