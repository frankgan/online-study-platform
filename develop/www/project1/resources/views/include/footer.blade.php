
{{-- style --}}

<style>

  .footer {
    position: relative;
    width: 100%;
    background: #202029;
    border-radius: 6px;
    font-family: 'Open Sans', sans-serif;
  }

  .footer .footer-row {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
    gap: 9rem;
    padding: 60px;
  }

  .footer-row .footer-col h4 {
    color: #fff;
    font-size: 1.2rem;
    font-weight: 400;
  }

  .footer-col .links {
    margin-top: 20px;
  }

  .footer-col .links li {
    list-style: none;
    margin-bottom: 10px;
  }

  .footer-col .links li a {
    text-decoration: none;
    color: #bfbfbf;
  }

  .footer-col .links li a:hover {
    color: #fff;
  }

  .footer-col p {
    margin: 20px 0;
    color: #bfbfbf;
    max-width: 300px;
  }

  .footer-col form {
    display: flex;
    gap: 5px;
  }

  .footer-col input {
    height: 40px;
    border-radius: 6px;
    background: none;
    width: 100%;
    outline: none;
    border: 1px solid #7489C6 ;
    caret-color: #fff;
    color: #fff;
    padding-left: 10px;
  }

  .footer-col input::placeholder {
    color: #ccc;
  }

  .footer-col form button {
    background: #fff;
    outline: none;
    border: none;
    padding: 10px 15px;
    border-radius: 6px;
    cursor: pointer;
    font-weight: 500;
    transition: 0.2s ease;
  }

  .footer-col form button:hover {
    background: #cecccc;
  }

  .footer-col .icons {
    display: flex;
    margin-top: 30px;
    gap: 30px;
    cursor: pointer;
  }

  .footer-col .icons i {
    color: #afb6c7;
  }

  .footer-col .icons i:hover  {
    color: #fff;
  }

  .copy-right {
    width: 100%;
    padding: 1rem;
    text-align: center;
    font-size: .8rem;
    background: #0c0c10;
    color: #fff;
  }

  @media (max-width: 768px) {
    .footer {
      position: relative;
      bottom: 0;
      left: 0;
      transform: none;
      width: 100%;
      border-radius: 0;
    }

    .footer .footer-row {
      padding: 3rem 1rem 1.8rem;
      display: grid;
      gap: 0;
      grid-template-columns: repeat(3, 1fr);
      justify-items: center;
    }

    .footer-col form {
      display: block;
    }

    .footer-col form :where(input, button) {
      width: 100%;
    }

    .footer-col form button {
      margin: 10px 0 0 0;
    }

  }
</style>

<footer class="footer">

  <div class="footer-row">
    <div class="footer-col">
      <h4>認識 QUANTUM</h4>
      <ul class="links">
        <li><a href="#">關於 QUANTUM</a></li>
        <li><a href="#">加入我們</a></li>
      </ul>
    </div>
    <div class="footer-col">
      <h4>課程分類 (255)</h4>
      <ul class="links">
        <li><a href="#">平面設計 (33)</a></li>
        <li><a href="#">軟體開發 (21)</a></li>
        <li><a href="#">影像處理 (38)</a></li>
        <li><a href="#">3D 建模 (20)</a></li>
        <li><a href="#">攝影與攝像 (22)</a></li>
        <li><a href="#">音樂製作 (28)</a></li>
      </ul>
    </div>
    <div class="footer-col">
      <h4>使用條款</h4>
      <ul class="links">
        <li><a href="#">使用條款</a></li>
        <li><a href="#">隱私政策</a></li>
        <li><a href="#">Cookie政策</a></li>
      </ul>
    </div>
  </div>

  <div class="copy-right">
    技術由 阿循 提供
  </div>

</footer>