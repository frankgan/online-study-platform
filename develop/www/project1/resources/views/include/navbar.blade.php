
<!-- style -->
<style>

  /* navbar */
  nav.navbar {
    display: flex;
    position: sticky;
    top: 0;
    align-items: center;
    color: #fff;
    background: #27262D;
    padding: 0 2rem;
  }

  nav.navbar > div.menu {
    display: flex;
    justify-content: space-between;
    align-items: center;
    position: relative;
  }

  nav.navbar > div.menu > ul.list {
    display: flex;
    column-gap: 0;
  }

  nav.navbar > div.menu > ul.list li.item {
    padding: 1.2rem 1rem;
    cursor: pointer;
    font-size: .9rem;
  }

  nav.navbar > div.menu > ul.list li.item:hover {
    color: #ddd;
    background:#17171C;
  }

  #menu-label {
    display: none;
  }

  input {
    display: none;
  }

  /* mobile */
  @media screen and (max-width: 768px) {

    nav.navbar {
      padding: 1.4rem 2rem;
    }
    

    nav.navbar > div.menu > ul.list {
      flex-direction: column;
      position: fixed;
      height: 100vh;
      top: 3.82rem;
      left: -100%;
      background: #27262D;
      box-shadow: inset 0 0 20px #17171C;
      transition: all .27s ease-out;
    }

    nav.navbar > div.menu > input[type="checkbox"]#menu-state:checked + ul.list {
      left: 0;
    }

    nav.navbar > div.menu > ul.list > li.item {
      padding: 1.2rem 5rem;
      text-align: center;
    }

    #menu-label {
      display: block;
      position: relative;
      font-size: 1.1rem;
      cursor: pointer;
    }

  }

</style>

<nav class="navbar">

  <label id="menu-label" for="menu-state">
    <i class="menu-button fa fa-bars"></i>
  </label>

  <div class="menu">

    <input type="checkbox" id="menu-state">

    <ul class="list">

      <li class="item">
        <a href="#">首頁</a>
      </li>
  
      <li class="item">
        <a href="#">課程資訊</a>
      </li>
  
      <li class="item">
        <a href="#">關於我們</a>
      </li>
  
      <li class="item">
        <a href="#">問題回報</a>
      </li>

    </ul>

  </div>

</nav>