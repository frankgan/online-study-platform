
{{-- style --}}
<style>

    section#app {
        padding: 2rem;
    }

    section#app > div#event {
        display: grid;
        gap: 2rem;
        grid-template-columns: repeat(3, 1fr);
    }

    @media screen and (max-width: 768px) {
        
        section#app > div#event {
            grid-template-columns: repeat(1, 1fr);
        }

    }

</style>

@extends('layout.app')

@section('title', "首頁")

@section('app')

    <section id="app">

        
        <div id="event">

            <x-event-box>
                <x-slot name="img">./images/events/phone-1.jpg</x-slot>
                <x-slot name="title">在非洲被獵豹追</x-slot>
                <x-slot name="date">2024年5月15日</x-slot>
                Loru
            </x-event-box>

            <x-event-box>
                <x-slot name="img">./images/events/phone-2.jpg</x-slot>
                <x-slot name="title">我也不知道這是衝三毀</x-slot>
                <x-slot name="date">2024年6月12日</x-slot>
                Loru
            </x-event-box>

        </div>

    </section>

@endsection